def code_morze(value):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    morse_dict = {
        'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.', 'G': '--.',
        'H': '....', 'I': '..', 'J': '.---', 'K': '-.-', 'L': '.-..', 'M': '--', 'N': '-.',
        'O': '---', 'P': '.--.', 'Q': '--.-', 'R': '.-.', 'S': '...', 'T': '-',
        'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-', 'Y': '-.--', 'Z': '--..',
        '1': '.----', '2': '..---', '3': '...--', '4': '....-', '5': '.....',
        '6': '-....', '7': '--...', '8': '---..', '9': '----.', '0': '-----',
        ', ': '--..--', '.': '.-.-.-', '?': '..--..', '/': '-..-.', '-': '-....-',
        '(': '-.--.', ')': '-.--.-'
    }

    # Convert the text to uppercase and split it into words
    words = value.upper().split()

    # Initialize an empty list to store the Morse code for each word
    morse_code_list = []

    for word in words:
        # Initialize an empty string to store the Morse code for the current word
        morse_word = ""

        for char in word:
            if char in morse_dict:
                morse_word += morse_dict[char] + " "

        # Remove the trailing space and add the Morse code for the word to the list
        morse_code_list.append(morse_word.strip())

    # Join the Morse code for words with a space and return the result
    return " ".join(morse_code_list)


